package com.example.anro.imageprocessorjava.models;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.anro.imageprocessorjava.async_tasks.SlowImageProcessingTask;
import com.example.anro.imageprocessorjava.glide.ImageUtils;
import com.example.anro.imageprocessorjava.MainActivity;
import com.example.anro.imageprocessorjava.R;
import com.example.anro.imageprocessorjava.interfaces.Observer;

import java.util.ArrayList;

public class PreviewAdapter extends RecyclerView.Adapter <PreviewAdapter.PreviewViewHolder>{

    private static final String LOG_TAG = MainActivity.LOG_TAG;

    private View.OnClickListener mOnItemClickListener;
    private ArrayList<ImageItem> mDataSet;

    private ArrayList<SlowImageProcessingTask> mTasksPool;
    private int mPreviewHeight;
    private int mPreviewWidth;
    private final Observer mObserver;

    public PreviewAdapter(ArrayList<ImageItem> mDataSet, Observer observer) {
        this.mDataSet = mDataSet;
        mObserver = observer;
    }

    public PreviewAdapter(ImageItem imageItem, Observer observer){
        mDataSet = new ArrayList<>();
        mDataSet.add(imageItem);
        mObserver = observer;
    }

    @NonNull
    @Override
    public PreviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PreviewViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.preview_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PreviewViewHolder holder, int position) {
        ImageItem item = mDataSet.get(position);
        if (MainActivity.sIsSlowLoading && item.isLoading()) {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.INVISIBLE);
            holder.progressBar.setProgress(item.getProgress());
            if (!containsTaskWithItem(item)) {
                if (mPreviewHeight == 0 || mPreviewWidth == 0) {
                    holder.itemView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            startTaskTransformation(holder.imageView.getWidth(), holder.imageView.getHeight(), item, holder.itemView.getContext());
                            holder.itemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    });
                } else {
                    startTaskTransformation(mPreviewWidth, mPreviewHeight, item, holder.itemView.getContext());
                }
            }
        }
        else {
            holder.progressBar.setVisibility(View.INVISIBLE);
            holder.imageView.setVisibility(View.VISIBLE);
            ImageUtils.loadImage(item, holder.imageView);
            holder.itemView.setOnClickListener(mOnItemClickListener);
            holder.itemView.setTag(item);
        }
        int colorResBg = (position+1)%2 == 0? R.color.image_background_even : R.color.image_background_uneven;
        Log.i(LOG_TAG, "position " + (position+1) +" is even? " + Boolean.valueOf((position+1)%2 == 0));
        holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), colorResBg));
    }

    private void startTaskTransformation(int width, int height, ImageItem item, Context context){
        if (mPreviewHeight == 0 || mPreviewWidth == 0){
            mPreviewHeight = height;
            mPreviewWidth = width;
        }
        if (mTasksPool == null){
            mTasksPool = new ArrayList<>();
        }
        SlowImageProcessingTask task = new SlowImageProcessingTask(context, mObserver, item, width, height);
        mTasksPool.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    @Override
    public int getItemCount() {
        if (mDataSet != null) return mDataSet.size();
        return 0;
    }

    public void append(ImageItem imageItem){
        if (mDataSet != null && !mDataSet.contains(imageItem)){
            mDataSet.add(imageItem);
            notifyItemInserted(mDataSet.size()-1);
        }
    }

    public void remove(ImageItem imageItem){
        if (mDataSet != null){
            int index = mDataSet.indexOf(imageItem);
            if (index != -1){
                mDataSet.remove(index);
                notifyItemRemoved(index);
                if (index < mDataSet.size()) {
                    for (int i = index; i < mDataSet.size(); i++) {
                        notifyItemChanged(i);
                    }
                }
            }
//            notifyDataSetChanged();
        }
    }

    public void update(ImageItem imageItem){
        if (mDataSet != null){
            int index = mDataSet.indexOf(imageItem);
            if (index != -1){
                mDataSet.get(index).setProgress(imageItem.getProgress());
                mDataSet.get(index).setLoading(imageItem.isLoading());
                notifyItemChanged(index);
            }
        }
    }

    public boolean containsTaskWithItem(ImageItem item){
        if (mTasksPool != null && item != null){
            for (SlowImageProcessingTask task:mTasksPool) {
                if (item.equals(task.getImageItem())){
                    return true;
                }
            }
        }
        return false;
    }

    public void restartLoadForItems(ArrayList<ImageItem> items, Context context){
        if (items != null && mDataSet != null && context != null) {
            for (ImageItem item : items) {
                int index = mDataSet.indexOf(item);
                if (index != -1){
                    ImageItem originalItem = mDataSet.get(index);
                    originalItem.setLoading(true);
                    originalItem.setProgress(0);
                    notifyItemChanged(index);
                    startTaskTransformation(mPreviewWidth, mPreviewHeight, originalItem, context);
                }
            }
        }
    }

    public ArrayList<SlowImageProcessingTask> getTasksPool() {
        return mTasksPool;
    }

    public ArrayList<ImageItem> getDataSet() {
        return mDataSet;
    }

    public void setOnItemClickListener(View.OnClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    class PreviewViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;

        PreviewViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.id__image_view);
            progressBar = itemView.findViewById(R.id.id__progress_bar);
        }
    }
}
