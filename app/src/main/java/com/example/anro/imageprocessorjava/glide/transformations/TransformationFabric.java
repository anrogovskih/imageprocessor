package com.example.anro.imageprocessorjava.glide.transformations;

import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

public class TransformationFabric {

    private static Rotation mRotationTransformation;
    private static Mirror mMirrorTransformation;
    private static BlackAndWhite mInvertColorsTransformation;

    private TransformationFabric(){

    }

    public static BitmapTransformation getTransformation(TransformationsType type){
        if (type != null) {
            switch (type) {
                case MIRROR:
                    if (mMirrorTransformation == null) {
                        mMirrorTransformation = new Mirror();
                    }
                    return mMirrorTransformation;
                case ROTATE:
                    if (mRotationTransformation == null) {
                        mRotationTransformation = new Rotation(90);
                    }
                    return mRotationTransformation;
                case BLACK_AND_WHITE:
                    if (mInvertColorsTransformation == null) {
                        mInvertColorsTransformation = new BlackAndWhite();
                    }
                    return mInvertColorsTransformation;
            }
        }
        return null;
    }
}
