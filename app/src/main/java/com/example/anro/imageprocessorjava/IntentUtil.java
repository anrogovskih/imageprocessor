package com.example.anro.imageprocessorjava;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;

import java.util.List;


/**
 * Created by rogovskih.a on 09.02.2017.
 */

public class IntentUtil {
    public static boolean isImplicitIntentCallable(Intent intent, Activity activity) {
        List<ResolveInfo> list = activity.getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public static void openInBrowser(Activity activity, String url){
        if (activity != null && url != null){
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        }
    }

    public static void goToAppSettings(Activity activity){
        if (activity != null){
            activity.startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", activity.getPackageName(), null)));
        }
    }
}
