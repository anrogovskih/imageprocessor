package com.example.anro.imageprocessorjava;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.example.anro.imageprocessorjava.models.ImageItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class FilesUtils {

    public static String getFileName(){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(System.currentTimeMillis());
        return  "img_" + timeStamp + ".jpg";
    }

    public static ImageItem saveToTempFile(Bitmap bitmap, Activity activity){
        File file = new File(activity.getFilesDir(), FilesUtils.getFileName());
        return saveToFile(bitmap, activity, file);
    }

    public static ImageItem saveToPersistantFile(Bitmap bitmap, Activity activity){
        File file = createImageFile();
        return saveToFile(bitmap, activity, file);
    }

    public static ImageItem saveToFile(Bitmap bitmap, Activity activity, File file){
        if (activity != null && file != null) {

            FileOutputStream outStream;
            try {
                outStream = activity.openFileOutput(file.getName(), Context.MODE_PRIVATE);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();
                return new ImageItem(activity.getFileStreamPath(file.getName()).getAbsolutePath(), false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static File createImageFile(){
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        root.mkdirs();
        return new File(root, FilesUtils.getFileName());
    }
}
