package com.example.anro.imageprocessorjava.interfaces;

public interface Observer {

    void update(Object observable, Object data);
}
