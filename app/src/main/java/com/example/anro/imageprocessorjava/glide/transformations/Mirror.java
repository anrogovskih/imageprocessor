package com.example.anro.imageprocessorjava.glide.transformations;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.example.anro.imageprocessorjava.MainActivity;

import java.security.MessageDigest;

public class Mirror extends BitmapTransformation {
    private static final int VERSION = 14;

    private static final String ID = "com.example.anro.imageprocessorjava.glide.transformations.Mirror." + VERSION;
    private static final byte[] ID_BYTES = ID.getBytes(CHARSET);

    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
        Log.i(MainActivity.LOG_TAG, "Mirror transform");
        int width = toTransform.getWidth();
        int height = toTransform.getHeight();

        Bitmap.Config config =
                toTransform.getConfig() != null ? toTransform.getConfig() : Bitmap.Config.ARGB_8888;
        Bitmap bitmap = pool.get(width, height, config);

        Canvas canvas = new Canvas(bitmap);

        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);
        matrix.postTranslate(width, 0);
        canvas.drawBitmap(toTransform, matrix, null);

        return bitmap;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Mirror;
    }

    @Override
    public int hashCode() {
        return ID.hashCode();
    }


    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update(ID_BYTES);
    }
}
