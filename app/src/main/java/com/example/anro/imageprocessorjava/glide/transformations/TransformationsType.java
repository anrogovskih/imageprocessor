package com.example.anro.imageprocessorjava.glide.transformations;

public enum TransformationsType {
    ROTATE,
    BLACK_AND_WHITE,
    MIRROR,

    NONE
}
