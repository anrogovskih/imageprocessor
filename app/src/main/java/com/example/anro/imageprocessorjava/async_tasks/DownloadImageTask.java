package com.example.anro.imageprocessorjava.async_tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.bumptech.glide.request.target.Target;
import com.example.anro.imageprocessorjava.MainActivity;
import com.example.anro.imageprocessorjava.glide.GlideApp;
import com.example.anro.imageprocessorjava.glide.ImageUtils;
import com.example.anro.imageprocessorjava.interfaces.Observer;

import java.lang.ref.WeakReference;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    private WeakReference<Context> contextWeakReference;
    private Observer observer;

    public DownloadImageTask(Observer observer, Context context) {
        this.observer = observer;
        contextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String url = params[0];
        try {
            return GlideApp
                    .with(contextWeakReference.get())
                    .asBitmap()
                    .load(url)
                    .placeholder(ImageUtils.getInstance().getPlaceholder(contextWeakReference.get()))
                    .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get(); // needs to be called on background thread
        } catch (Exception ex) {
            Log.e(MainActivity.LOG_TAG, "Downloading " + url + " failed", ex);
            return null;
        }
    }
    @Override
    protected void onPostExecute(Bitmap result) {
        if (observer == null) { return; }
        observer.update(this, result);
    }
}
