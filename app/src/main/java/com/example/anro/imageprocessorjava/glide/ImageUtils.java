package com.example.anro.imageprocessorjava.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.RequestListener;
import com.example.anro.imageprocessorjava.R;
import com.example.anro.imageprocessorjava.glide.transformations.TransformationFabric;
import com.example.anro.imageprocessorjava.models.ImageItem;


import java.io.File;
import java.util.Random;


import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by rogovskih.a
 * Date is 08.02.2017.
 *
 * All rights reserved by Laximo!
 *
 * Класс, содержащий public static методы для работы с изображениями
 */

public class ImageUtils {

    private static ImageUtils instance;
    private Random random;
    private ColorDrawable placeholder;

    private ImageUtils() {
    }

    public static ImageUtils getInstance(){
        if (instance == null){
            instance = new ImageUtils();
        }
        return instance;
    }

    public Random getRandom() {
        if (random == null){
            random = new Random();
        }
        return random;
    }

    public ColorDrawable getPlaceholder(Context context) {
        if (placeholder == null){
            placeholder = new ColorDrawable(ContextCompat.getColor(context, R.color.image_placeholder));
        }
        return placeholder;
    }

    private static void loadImage(String uri,
                                  ImageView image,
                                  boolean isFromCamera,
                                  RequestListener<Bitmap> listener) {
        if (image == null) return;
        Object source = isFromCamera ? new File(uri) : uri;
        GlideApp.with(image.getContext())
                .asBitmap()
                .load(source)
                .transform(new FitCenter())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(getInstance().getPlaceholder(image.getContext()))
                .listener(listener)
                .into(image);
    }

    public static void loadImage(ImageItem imageItem, ImageView image){
        if (image == null || imageItem == null) return;
        loadImage(imageItem, image, null);
    }

    public static void loadImage(ImageItem imageItem, ImageView image, RequestListener<Bitmap> listener){
        if (image == null || imageItem == null) return;
        BitmapTransformation transformation = TransformationFabric.getTransformation(imageItem.getTransformationsType());
        if (transformation == null){
            loadImage(imageItem.getUrl(), image, imageItem.isFromCamera(), listener);
        }
        else {
            Object source = imageItem.isFromCamera()? new File(imageItem.getUrl()) : imageItem.getUrl();
            GlideApp.with(image.getContext())
                    .asBitmap()
                    .load(source)
                    .transform(new MultiTransformation<>(new FitCenter(), transformation))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(getInstance().getPlaceholder(image.getContext()))
                    .listener(listener)
                    .into(image);
        }
    }
}
