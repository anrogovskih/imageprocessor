package com.example.anro.imageprocessorjava.async_tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.target.Target;
import com.example.anro.imageprocessorjava.MainActivity;
import com.example.anro.imageprocessorjava.glide.GlideApp;
import com.example.anro.imageprocessorjava.glide.ImageUtils;
import com.example.anro.imageprocessorjava.glide.transformations.TransformationFabric;
import com.example.anro.imageprocessorjava.interfaces.Observer;
import com.example.anro.imageprocessorjava.models.ImageItem;

import java.lang.ref.WeakReference;
import java.util.Objects;

public class SlowImageProcessingTask extends AsyncTask<Void, Integer, Bitmap> {
    private static final int MIN_DURATION = 5;
    private static final int MAX_DURATION = 30;

    private WeakReference<Context> contextWeakReference;
    private Observer observer;
    private ImageItem imageItem;
    private double taskDuration;
    private int targetWidth = Target.SIZE_ORIGINAL;
    private int targetHeight = Target.SIZE_ORIGINAL;

    public SlowImageProcessingTask(Context context,
                                   Observer observer,
                                   ImageItem imageItem,
                                   int targetWidth,
                                   int targetHeight) {
        this.contextWeakReference = new WeakReference<>(context);
        this.observer = observer;
        this.imageItem = imageItem;
        if (targetHeight != 0 && targetWidth != 0) {
            this.targetWidth = targetWidth;
            this.targetHeight = targetHeight;
        }
        taskDuration = ImageUtils.getInstance().getRandom().nextInt(MAX_DURATION - MIN_DURATION + 1) + MIN_DURATION;
        Log.i(MainActivity.LOG_TAG, "SlowImageProcessingTask constructor: taskDuration is " +taskDuration);
    }

    public ImageItem getImageItem() {
        return imageItem;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            if (imageItem != null) {
                BitmapTransformation transformation = TransformationFabric.getTransformation(imageItem.getTransformationsType());
                if (transformation != null) {
                    for (double i = 0; i < taskDuration; i++) {
                        if (isCancelled()) return null;
                        publishProgress((int) ((i / taskDuration) * 100));
                        Thread.sleep(1000);
                    }
                    return GlideApp
                            .with(contextWeakReference.get())
                            .asBitmap()
                            .load(imageItem.getUrl())
                            .transform(new MultiTransformation<>(new FitCenter(), transformation))
                            .submit(targetWidth, targetHeight)
                            .get(); // needs to be called on background thread
                }
            }
        } catch (Exception ex) {
            Log.e(MainActivity.LOG_TAG, "Downloading " + imageItem.getUrl() + " failed", ex);
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (observer == null) { return; }
        observer.update(this, values[0]);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (observer == null) { return; }
        observer.update(this, result);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SlowImageProcessingTask task = (SlowImageProcessingTask) o;
        return Objects.equals(imageItem, task.imageItem);
    }

    @Override
    public int hashCode() {

        return Objects.hash(imageItem);
    }
}
