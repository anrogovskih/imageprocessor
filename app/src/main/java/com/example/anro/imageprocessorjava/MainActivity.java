package com.example.anro.imageprocessorjava;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.anro.imageprocessorjava.async_tasks.DownloadImageTask;
import com.example.anro.imageprocessorjava.async_tasks.SlowImageProcessingTask;
import com.example.anro.imageprocessorjava.glide.GlideApp;
import com.example.anro.imageprocessorjava.glide.ImageUtils;
import com.example.anro.imageprocessorjava.glide.transformations.TransformationsType;
import com.example.anro.imageprocessorjava.interfaces.Observer;
import com.example.anro.imageprocessorjava.models.ImageItem;
import com.example.anro.imageprocessorjava.models.PreviewAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity implements Observer {

    //TODO: обработать клик по главному изображению, если ещё ничего не прогрузилось
    //TODO: удалять файлы, как только не осталось ни одной ссылки на них
    //TODO: добавить отступы к полю ввода url

    public static final int REQUEST_CODE_CAMERA = 0;
    public static final int REQUEST_CODE_STORAGE = 1;

    public static final String LOG_TAG = "LOG_TAG" /*MainActivity.class.getSimpleName()*/;
    private static final String TAG_MAIN_ITEM = "TAG_MAIN_ITEM";
    private static final String TAG_ITEMS = "TAG_ITEMS";
    private static final String AUTHORITY = "com.example.anro.imageprocessorjava.provider.GenericFileProvider";

    private static String sCurrentPhotoPath;
    public static boolean sIsSlowLoading;

//    String url = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Hopetoun_falls.jpg/1920px-Hopetoun_falls.jpg";
    //TODO: заглушка, удалить

    private Button mLoadImageButton;
    private Button mFirstFilterButton;
    private Button mSecondFilterButton;
    private Button mThirdFilterButton;
    private RecyclerView mRecyclerView;
    private ImageView mImageView;
    private TextView mEmptyStateTextView;
    private ProgressBar mProgressBar;
    private CheckBox mSlowLoadingCheck;

    private PreviewAdapter mAdapter;
    private ImageItem mMainImageItem;
    private ArrayList<ImageItem> mPendingItems;

    //слушатель окончания загрузки изображения, сохряняет результат во временный файл, который будет удален при завершении Activity
    private RequestListener<Bitmap> mRequestListenerSaveToFile = new RequestListener<Bitmap>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
            showErrorToast();
            return false;
        }

        @Override
        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
            mMainImageItem = FilesUtils.saveToTempFile(resource, MainActivity.this);
            //TODO: подумать что можно сделать, если не удалось сохранить
            return false;
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMainImageItem != null) {
            Gson gson = new Gson();
            String jsonMainItem = gson.toJson(mMainImageItem);
            outState.putString(TAG_MAIN_ITEM, jsonMainItem);

            if (mAdapter != null && mAdapter.getDataSet() != null && !mAdapter.getDataSet().isEmpty()) {
                String jsonItems = gson.toJson(mAdapter.getDataSet());
                outState.putString(TAG_ITEMS, jsonItems);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLoadImageButton = findViewById(R.id.id__button_load_image);
        mFirstFilterButton = findViewById(R.id.id__button_filter_1);
        mSecondFilterButton = findViewById(R.id.id__button_filter_2);
        mThirdFilterButton = findViewById(R.id.id__button_filter_3);
        mRecyclerView = findViewById(R.id.id__preview_container);
        mImageView = findViewById(R.id.id__image_view);
        mEmptyStateTextView = findViewById(R.id.id__empty_state_tv);
        mProgressBar = findViewById(R.id.id__progress_bar);
        mSlowLoadingCheck = findViewById(R.id.id__check_box);

        mLoadImageButton.setOnClickListener((View v)-> showDialogImageSource());
        mImageView.setOnClickListener((View v)-> showDialogImageSource());
        mFirstFilterButton.setOnClickListener((View v)-> applyFilterRotate());
        mSecondFilterButton.setOnClickListener((View v)-> applyFilterInvertColors());
        mThirdFilterButton.setOnClickListener((View v)-> applyFilterMirror());
        mSlowLoadingCheck.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked)-> sIsSlowLoading = isChecked);
        mSlowLoadingCheck.setChecked(true);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            Gson gson = new Gson();

            if (savedInstanceState.get(TAG_MAIN_ITEM) != null){
                mMainImageItem = gson.fromJson(savedInstanceState.getString(TAG_MAIN_ITEM), ImageItem.class);
                mMainImageItem.setProgress(0);
                loadMainImage(mMainImageItem);
            }

            if (savedInstanceState.get(TAG_ITEMS) != null) {
                Type type = new TypeToken<ArrayList<ImageItem>>() {}.getType();
                ArrayList<ImageItem> previews = gson.fromJson(savedInstanceState.getString(TAG_ITEMS), type);
                for (ImageItem item:previews) {
                    item.setProgress(0);
                }
                mAdapter = new PreviewAdapter(previews, this);
                setAdapter(mAdapter);
                mEmptyStateTextView.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null && mAdapter.getTasksPool() != null && !mAdapter.getTasksPool().isEmpty()){
            mPendingItems = new ArrayList<>();
            for (SlowImageProcessingTask task : mAdapter.getTasksPool()) {
                task.cancel(true);
                mPendingItems.add(task.getImageItem());
            }
            mAdapter.getTasksPool().clear();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mPendingItems != null && !mPendingItems.isEmpty() && mAdapter != null){
            mAdapter.restartLoadForItems(mPendingItems, this);
            mPendingItems.clear();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()){
            for (String s:fileList()) {
                deleteFile(s);
            }
        }
    }

    /**
     * Показать диалог с выбором варианта откуда загрузить изображение
     */
    private void showDialogImageSource(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.string__dialog_pick_image__title);
        builder.setPositiveButton(R.string.string__dialog_pick_image__button_storage, (DialogInterface dialog, int id)->loadFromFile(dialog));
        //check if the device has camera to handle this option
        if (IntentUtil.isImplicitIntentCallable(new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE), this)) {
            builder.setNegativeButton(R.string.string__dialog_pick_image__button_camera, (DialogInterface dialog, int id) -> loadFromCamera(dialog));
        }
        builder.setNeutralButton(R.string.string__dialog_pick_image__button_net, (DialogInterface dialog, int id)->{
            dialog.dismiss();
            showDialogEnterUrl();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Диалог, который показываем, если разрешение было отклонено с галкой "не спрашивать больше"
     * @param permissionName - имя разрешения, которое нужно включить
     */
    private void showDialogTurnOnPermission(String permissionName){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(getString(R.string.string__dialog_go_to_settings__title, permissionName));
        builder.setPositiveButton(
                R.string.string__dialog_go_to_settings__button,
                (DialogInterface dialog, int id)-> {
                    dialog.dismiss();
                    IntentUtil.goToAppSettings(MainActivity.this);
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Диалог по клику на превью
     * @param view, по которой кликнули, чтобы вызвать этот диалог
     */
    private void showDialogOnPreviewClicked(View view){
        if (view.getTag() instanceof ImageItem){
            ImageItem imageItem = (ImageItem) view.getTag();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle(R.string.string__dialog_on_preview_clicked__title);
            builder.setPositiveButton(
                    R.string.string__dialog_on_preview_clicked__button_filter,
                    (DialogInterface dialogInterface, int i) -> setToTheMainImage(dialogInterface));
            builder.setNegativeButton(
                    R.string.string__dialog_on_preview_clicked__button_delete,
                    (DialogInterface dialogInterface, int i) -> deletePreviewItem(dialogInterface));

            AlertDialog dialog = builder.create();
            dialog.show();

            dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTag(imageItem);
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTag(imageItem);
        }
    }

    /**
     * Диалог с полем ввода для ввода адреса url изображения
     */
    private void showDialogEnterUrl(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.string__dialog_enter_url__title);

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        int margin = (int)getResources().getDimension(R.dimen.dimen__default_margin);
        lp.leftMargin = margin;
        lp.rightMargin = margin;
        input.setLayoutParams(lp);
//        input.setText(url);
        builder.setView(input);

        builder.setPositiveButton(
                R.string.string__dialog_enter_url__button_ok,
                (DialogInterface dialogInterface, int i) -> {
                    dialogInterface.dismiss();
                    loadFromNet(input.getText().toString());
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Удалить элемент, по которому кликнули, из списка превью
     */
    private void deletePreviewItem(DialogInterface dialogInterface){
        Object tag = ((AlertDialog)dialogInterface).getButton(DialogInterface.BUTTON_NEGATIVE).getTag();
        if (tag instanceof ImageItem && mAdapter != null){
            mAdapter.remove((ImageItem)tag);
            onPreviewDeleted((ImageItem)tag);
        }
        dialogInterface.dismiss();
    }

    /**
     * Установить отфильтрованное изображение из превью в основное ImageView
     */
    private void setToTheMainImage(DialogInterface dialogInterface){
        dialogInterface.dismiss();

        Button positiveButton = ((AlertDialog)dialogInterface).getButton(DialogInterface.BUTTON_POSITIVE);
        Object tag = positiveButton.getTag();
        if (tag instanceof ImageItem){
            ImageItem imageItem = (ImageItem)tag;
            ImageUtils.loadImage(imageItem, mImageView, mRequestListenerSaveToFile);
        }
    }

    /**
     * Применить фильтр поворота на 90 градусов
     */
    private void applyFilterRotate(){
        addPreview(getFilteredItem(TransformationsType.ROTATE));
    }

    /**
     * Применить черно-белый фильтр
     */
    private void applyFilterInvertColors(){
        addPreview(getFilteredItem(TransformationsType.BLACK_AND_WHITE));
    }

    /**
     * Применить фильтр зеркального отображения
     */
    private void applyFilterMirror(){
        addPreview(getFilteredItem(TransformationsType.MIRROR));
    }

    /**
     * Инициализировать новый объект {@link ImageItem} на основе основного изображения
     * @param transformationsType - фильтр, который нужно будет применить к основному изображению
     * @return клон mMainImageItem с новым фильтром
     */
    private ImageItem getFilteredItem(TransformationsType transformationsType){
        if (mMainImageItem != null){
            try {
                ImageItem imageItem = mMainImageItem.clone();
                imageItem.setTransformationsType(transformationsType);
                return imageItem;
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Добавить переданный объект {@link ImageItem} в список превью
     */
    private void addPreview(ImageItem imageItem){
        if (!isConnected()) {
            showToastNoInternet();
            return;
        }
        if (imageItem != null) {
            if (mAdapter == null) {
                mAdapter = new PreviewAdapter(imageItem, this);
                setAdapter(mAdapter);
            } else {
                mAdapter.append(imageItem);
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount()-1);
            }
            if (mEmptyStateTextView != null && mEmptyStateTextView.isShown()){
                mEmptyStateTextView.setVisibility(View.INVISIBLE);
            }
        }
        else {
            showErrorToast();
        }
    }

    /**
     * Установить адаптер для RecyclerView
     */
    private void setAdapter(PreviewAdapter adapter){
        if (adapter != null && mRecyclerView != null){
            adapter.setOnItemClickListener((View view)->showDialogOnPreviewClicked(view));
            mRecyclerView.setAdapter(adapter);
        }
    }

    /**
     * Загрузить изображение с камеры или запросить разрешение
     */
    private void loadFromCamera(DialogInterface dialog){
        if (dialog != null) dialog.dismiss();
        if (checkPermission(Manifest.permission.CAMERA, REQUEST_CODE_CAMERA)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = FilesUtils.createImageFile();

                // Continue only if the File was successfully created
                if (photoFile != null) {
                    sCurrentPhotoPath = Uri.fromFile(photoFile).getPath();

                    Uri photoURI = FileProvider.getUriForFile(this, AUTHORITY, photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_CODE_CAMERA);
                }
                else showErrorToast();
            }
        }
    }

    /**
     * Загрузить изображение с устройства или запросить разрешение
     */
    private void loadFromFile(DialogInterface dialog){
        if (dialog != null) dialog.dismiss();
        if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_CODE_STORAGE)) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, REQUEST_CODE_STORAGE);
        }
    }

    /**
     * Загрузить изображение из интернета
     * @param url - url, введенный пользователем
     */
    private void loadFromNet(String url){
        if (!isConnected()) {
            showToastNoInternet();
            return;
        }
        if (url != null){
            setLoadingState(true);
            new DownloadImageTask(this, this).execute(url);
        }
    }

    private boolean checkPermission(String permission, int requestCode){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (shouldShowRequestPermissionRationale(permission)) {
                    String permissionString = "";
                    switch (permission){
                        case Manifest.permission.CAMERA:
                            permissionString = "camera";
                            break;
                        case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                            permissionString = "write to external storage";
                            break;
                    }
                    showDialogTurnOnPermission(permissionString);
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {
                    // No explanation needed, we can request the permission.
                    requestPermissions(new String[]{permission}, requestCode);
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Загрузить изображение в основную ImageView
     * @param imageItem - объект, содержащий url
     */
    private void loadMainImage(ImageItem imageItem){
        if (!isConnected()) {
            showToastNoInternet();
            return;
        }
        if (imageItem != null && mImageView != null){
            mMainImageItem = imageItem;
            possiblyShowMainImageView();
            ImageUtils.loadImage(imageItem, mImageView);
        }
        else showErrorToast();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CAMERA:
                    if (sCurrentPhotoPath != null) {
                        loadMainImage(new ImageItem(sCurrentPhotoPath, true));
                    }
                    else showErrorToast();
                    break;
                case REQUEST_CODE_STORAGE:
                    Uri selectedImage = imageReturnedIntent.getData();
                    if (selectedImage != null) {
                        loadMainImage(new ImageItem(selectedImage.toString(), false));
                    }
                    else showErrorToast();
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case REQUEST_CODE_STORAGE:
                    loadFromFile(null);
                    break;
                case REQUEST_CODE_CAMERA:
                    loadFromCamera(null);
                    break;
            }
        } else {
            showToast(R.string.string__toast__on_permission_denied);
        }
    }


    private void possiblyShowMainImageView(){
        if (mImageView != null && mLoadImageButton != null && !mImageView.isShown()){
            mImageView.setVisibility(View.VISIBLE);
            mLoadImageButton.setVisibility(View.INVISIBLE);
        }
    }

    private void onPreviewDeleted(ImageItem imageItem){
        if (mAdapter != null && mAdapter.getItemCount() == 0){
            mEmptyStateTextView.setVisibility(View.VISIBLE);
        }
    }

    private void showToast(int stringRes){
        Toast.makeText(this, getString(stringRes), Toast.LENGTH_SHORT).show();
    }
    private void showErrorToast(){
        Toast.makeText(this, getString(R.string.string__toast__error), Toast.LENGTH_SHORT).show();
    }

    private void showToastNoInternet(){
        Toast.makeText(this, getString(R.string.string__toast__error), Toast.LENGTH_SHORT).show();
    }

    private void setLoadingState(boolean isLoading) {
        if (mProgressBar != null)
            mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
        if (mLoadImageButton != null && isLoading){
            mLoadImageButton.setVisibility(View.INVISIBLE);
        }
    }

    public boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        return true;
    }

    @Override
    public void update(Object observable, Object data) {
        if (isDestroyed() || isFinishing()) return;
        if (observable instanceof DownloadImageTask){
            if (data instanceof Bitmap){
                loadMainImage(FilesUtils.saveToPersistantFile((Bitmap)data, this));
                setLoadingState(false);
            }
            else {
                showErrorToast();
                if (mLoadImageButton != null) mLoadImageButton.setVisibility(View.VISIBLE);
                if (mImageView != null) mImageView.setVisibility(View.INVISIBLE);
            }
        }
        else if (observable instanceof SlowImageProcessingTask){
            SlowImageProcessingTask task = (SlowImageProcessingTask)observable;
            if (data instanceof Bitmap){
                //image loaded
                ImageItem imageItem = task.getImageItem();
                imageItem.setProgress(100);
                imageItem.setLoading(false);
                if (mAdapter != null){
                    mAdapter.update(imageItem);
                    if (mAdapter.getTasksPool() != null)
                        mAdapter.getTasksPool().remove(task);
                }
            }
            else if (data instanceof Integer){
                //image loading is in progress
                ImageItem imageItem = task.getImageItem();
                imageItem.setProgress((int)data);
                if (mAdapter != null)mAdapter.update(imageItem);
            }
        }
    }
}
