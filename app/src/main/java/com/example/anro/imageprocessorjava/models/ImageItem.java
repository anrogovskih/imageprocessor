package com.example.anro.imageprocessorjava.models;

import android.support.annotation.NonNull;

import com.example.anro.imageprocessorjava.MainActivity;
import com.example.anro.imageprocessorjava.glide.transformations.TransformationsType;

import java.util.Objects;

public class ImageItem implements Cloneable {
    private TransformationsType transformationsType = TransformationsType.NONE;
    private String url;
    private boolean isFromCamera;
    private boolean isLoading = MainActivity.sIsSlowLoading;
    private int progress;
    private int taskDuration;

    public ImageItem(@NonNull String url, boolean isFromCamera) {
        this.url = url;
        this.isFromCamera = isFromCamera;
    }

    public TransformationsType getTransformationsType() {
        return transformationsType;
    }

    public void setTransformationsType(TransformationsType transformationsType) {
        this.transformationsType = transformationsType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isFromCamera() {
        return isFromCamera;
    }

    public void setFromCamera(boolean fromCamera) {
        isFromCamera = fromCamera;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getTaskDuration() {
        return taskDuration;
    }

    public void setTaskDuration(int taskDuration) {
        this.taskDuration = taskDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageItem imageItem = (ImageItem) o;
        return isFromCamera == imageItem.isFromCamera &&
                transformationsType == imageItem.transformationsType &&
                Objects.equals(url, imageItem.url);
    }

    @Override
    public int hashCode() {

        return Objects.hash(transformationsType, url, isFromCamera);
    }

    @Override
    public ImageItem clone() throws CloneNotSupportedException {
        super.clone();
        ImageItem clone = new ImageItem(url, isFromCamera);
        clone.setTransformationsType(transformationsType);
        clone.setLoading(isLoading);
        clone.setProgress(progress);
        return clone;
    }
}
