package com.example.anro.imageprocessorjava.glide.transformations;

import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.TransformationUtils;
import com.example.anro.imageprocessorjava.MainActivity;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Objects;

public class Rotation extends BitmapTransformation {
    private static final String ID = "com.example.anro.imageprocessorjava.glide.transformations.Rotation";
    private static final byte[] ID_BYTES = ID.getBytes(Charset.forName("UTF-8"));

    private int mOrientation;

    public Rotation(int orientation) {
        mOrientation = orientation;
    }

    @Override
    protected Bitmap transform(@NonNull  BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
        Log.i(MainActivity.LOG_TAG, "Rotation transform");

        int exifOrientationDegrees = getExifOrientationDegrees(mOrientation);
        return TransformationUtils.rotateImageExif(pool, toTransform, exifOrientationDegrees);
    }

    private int getExifOrientationDegrees(int orientation) {
        int exifInt;
        switch (orientation) {
            case 90:
                exifInt = ExifInterface.ORIENTATION_ROTATE_90;
                break;
            default:
                exifInt = ExifInterface.ORIENTATION_NORMAL;
                break;
        }
        return exifInt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rotation rotation = (Rotation) o;
        return mOrientation == rotation.mOrientation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mOrientation) + ID.hashCode();
    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update(ID_BYTES);

        byte[] radiusData = ByteBuffer.allocate(4).putInt(mOrientation).array();
        messageDigest.update(radiusData);
    }
}
